var dataphongkham = [
    {
      "STT": 1,
      "Name": "Phòng khám Đa khoa khu vực Đồng Ky",
      "Latitude":  105.7824276,
      "Rating": 2.98,
      "Longtitude": 10.0126063,
      "address": "1883, Quốc lộ 91C, Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 2,
      "Name": "Phòng khám Đa khoa khu vực Vĩnh Hòa",
      "Latitude":  105.1804886,
      "Rating": 3.98,
      "Longtitude": 10.8801661,
      "address": "952; xã Vĩnh Hòa, ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 3,
      "Name": "Phòng khám Đa khoa khu vực Chợ Vàm",
      "Latitude":   105.3306814,
      "Rating": 4.98,
      "Longtitude": 10.702865 ,
      "address": "Thị trấn Chợ Vàm Phú Tân, ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 4,
      "Name": "Phòng khám Đa khoa khu vực Thạnh Mỹ Tây",
      "Latitude":   105.1473691,
      "Rating": 1.98,
      "Longtitude": 10.5445292,
      "address": "Ấp Đông Châu, xã Thạnh Mỹ Tây, huyện Châu Phú, ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 5,
      "Name": "Phòng khám Đa khoa khu vực Tịnh Biên",
      "Latitude": 104.9461162,
      "Rating": 2.98,
      "Longtitude": 10.6038778,
      "address": "Hữu Nghĩa, Xuân Biên, Tịnh Biên, ",
      "Number_of_beds": "",
      "area": ""
},{
      "STT": 6,
      "Name": "Phòng khám Đa khoa khu vực Chi Lăng",
      "Latitude":  105.01775397,
      "Rating": 3.98,
      "Longtitude":   10.5295468,
      "address": "Thị trấn Chi Lăng, Tịnh Biên, ",
      "Number_of_beds": "",
      "area": ""

},{
      "STT": 7,
      "Name": "Phòng khám Đa khoa khu vực Ba Chúc",
      "Latitude": 104.9089042,
      "Rating": 4.98,
      "Longtitude": 10.4943645,
      "address": "Đường Ngô Tự Lợi, thị trấn Ba Chúc, huyện Tri Tôn, ",
      "Number_of_beds": "",
      "area": ""

},{
      "STT": 8,
      "Name": "Phòng khám Đa khoa khu vực Long Giang",
      "Latitude": 105.4358333,
      "Rating": 4.98,
      "Longtitude": 10.4683333 ,
      "address": "Ấp Long Thạnh 2, xã Long Giang, huyện Chợ Mới, ",
      "Number_of_beds": "",
      "area": ""

},
{
      "STT": 9,
      "Name": "Phòng khám Đa khoa khu vực Mỹ Luông",
      "Latitude":  105.4937686,
      "Rating": 5.98,
      "Longtitude": 10.4956501 ,
      "address": "Ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, ",
      "Number_of_beds": "",
      "area": ""
}
,{
      "STT": 10,
      "Name": "Phòng khám Đa khoa khu vực Vĩnh Bình",
      "Latitude": 105.1843801,
      "Rating": 5.98,
      "Longtitude": 10.4204614,
      "address": "Số 2991 tổ 8 ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, ",
      "Number_of_beds": "",
      "area": ""
}
];
